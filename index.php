
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Primo Search Panel Demo</title>

    <!-- Latest compiled and minified CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/cosmo/bootstrap.min.css" rel="stylesheet" integrity="sha384-h21C2fcDk/eFsW9sC9h0dhokq5pDinLNklTKoxIZRUn3+hvmgQSffLLQ4G4l2eEr" crossorigin="anonymous">

    <style type="text/css">
      #tab-wrapper {
        max-width:800px;
      }
      .tab-content {
        border-left: 1px solid #ccc;
        border-right: 1px solid #ccc;
        border-bottom: 1px solid #ccc;
        padding: 20px;
      }
    </style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <nav class="navbar navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Primo Search Panel</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">Github Repository</a></li>
            <li><a href="">Contact</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container">
      <h1>Primo Search Panel</h1>
      <div id="tab-wrapper">
        <ul class="nav nav-tabs responsive" id="myTab">
          <li class="active"><a href="#primo" data-toggle="tab">Discovery Search  </a></li>
          <li><a href="#articles" data-toggle="tab">Articles+  </a></li>
          <li><a href="#catalog" data-toggle="tab">Books &amp; More</a></li>
        </ul>
        <div class="tab-content responsive">
          <div class="tab-pane active" id="primo">
            <div id="discov-faq-link">
              <a href="?width=510&height=280&inline=true#csearch-desc-wrapper" title="What is Discovery Search?" class="colorbox-inline inline">
                <i class="fa fa-question-circle fa-lg"></i>
              </a>
            </div>
            <?php include("discovery.php"); ?>
          </div>
          <div class="tab-pane" id="articles">
            <?php include("articles.php"); ?>
          </div>
          <div class="tab-pane" id="catalog">
            <?php include("books.php"); ?>
          </div>
        </div>
      </div>
    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

  <script language="javascript" type="text/javascript">
    function searchPrimo1() {
      var primoQueryValue1 = jQuery("#primoQueryTemp1").val().replace(/,/g," ");
      document.getElementById("primoQuery1").value = "any,contains," + primoQueryValue1;
      document.forms["primoSearchForm1"].submit();
     }
    function searchPrimo2() {
      var primoQueryValue2 = jQuery("#primoQueryTemp2").val().replace(/,/g," ");
      document.getElementById("primoQuery2").value = "any,contains," + primoQueryValue2;
      document.forms["primoSearchForm2"].submit();
     }
    function searchPrimo3() {
      var primoQueryValue3 = jQuery("#primoQueryTemp3").val().replace(/,/g," ");
      document.getElementById("primoQuery3").value = document.getElementById("exlidInput_scope_1").value + ",contains," + primoQueryValue3 + ",AND";
      document.forms["primoSearchForm3"].submit();
     }
    function searchPrimo4() {
      var primoQueryValue4 = jQuery("#primoQueryTemp4").val().replace(/,/g," ");
      document.getElementById("primoQuery4").value =  document.getElementById("exlidInput_scope_2").value + ",contains," + primoQueryValue4;
      document.forms["primoSearchForm4"].submit();
     }
  </script>

  </body>
</html>
