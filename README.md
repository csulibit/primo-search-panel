# README #


### What is this repository for? ###

* This repository provides sample for creating a Primo New UI search widget similar to those found on https://biblio.csusm.edu.
* 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

To set up a search widget you need to you to add the form HTML (i.e. look in articles.php) to somewhere between the <body> and </body> tags of your web page. In addition, you must add the following JavaScript somewhere on your page.

    function searchPrimo2() {
      var primoQueryValue2 = jQuery("#primoQueryTemp2").val().replace(/,/g," ");
      document.getElementById("primoQuery2").value = "any,contains," + primoQueryValue2;
      document.forms["primoSearchForm2"].submit();
     }

Please note that the function name above can be anything. However, you will need to call that function from your the unsubmit attribute of your search form.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact