<form action="https://primo-pmtna01.hosted.exlibrisgroup.com/primo-explore/search?"  enctype="application/x-www-form-urlencoded; charset=utf-8" method="get" id="primoSearchForm1" name="primoSearchForm1" role="search" target="_self"  onsubmit="searchPrimo1()" class="form-horizontal">
  <div class="form-group">
    <label for="primoQueryTemp1" class="sr-only">Discovery Search</label>
    <input type="hidden" name="institution" value="CALS_USM"/> 
    <input type="hidden" name="vid" value="CALS_USM"/> 
    <input type="hidden" name="tab" value="default_tab"/> 
    <input type="hidden" name="search_scope" value="default_scope"/> 
    <input type="hidden" name="onCampus" value="false"/> 
    <input type="hidden" id="primoQuery1" name="query" />
    <input type="hidden" id="indx" name="indx" value="1"/> 
    <input type="hidden" id="bulkSize" name="bulkSize" value="20"/> 
    <input type="hidden" id="dym" name="dym" value="true"/> 
    <input type="hidden" id="highlight" name="highlight" value="true"/> 
    <input type="hidden" id="displayField" name="displayField" value="title"/>
    <div class="col-sm-8">
      <input type="text" size="50" title="search_field" id="primoQueryTemp1" name="queryTemp" class="form-control" value="" /><br/>
    </div>
  <button type="button" title="Search" class="btn btn-info" id="goButton" value="Search" name="primosearch" onclick="searchPrimo1()"/> <i class="fa fa-search"></i> Search</button>
  </div>
</form>
