<form action="https://primo-pmtna01.hosted.exlibrisgroup.com/primo-explore/search?"  enctype="application/x-www-form-urlencoded; charset=utf-8" method="get" id="primoSearchForm2" name="primoSearchForm2" role="search" target="_self"  onsubmit="searchPrimo2()" class="form-horizontal">
  <div class="form-group">
    <label for="primoQueryTemp2" class="sr-only">Articles Search</label>
    <input type="hidden" name="institution" value="CALS_USM"/> 
    <input type="hidden" name="vid" value="CALS_USM"/> 
    <input type="hidden" name="tab" value="cals_usm_pci"/> 
    <input type="hidden" name="search_scope" value="CSUSM_PC"/> 
    <input type="hidden" name="onCampus" value="false"/> 
    <input type="hidden" id="primoQuery2" name="query" />
    <input type="hidden" id="indx" name="indx" value="1"/> 
    <input type="hidden" id="bulkSize" name="bulkSize" value="20"/> 
    <input type="hidden" id="dym" name="dym" value="true"/> 
    <input type="hidden" id="highlight" name="highlight" value="true"/> 
    <input type="hidden" id="displayField" name="displayField" value="title"/>
    <div class="col-sm-8">
      <input type="text" size="50" title="search_field" id="primoQueryTemp2" name="queryTemp" class="form-control" value="" />
      <input type="checkbox" id="facet" name="facet" value="tlevel,include,peer_reviewed"/> Search peer-reviewed only [experimental]
    </div>
    <button type="button" title="Search" class="btn btn-info" id="goButton" value="Search" name="primosearch" onclick="searchPrimo2()"/> <i class="fa fa-search"></i> Search</button>
  </div>
</form>
