<form action="https://primo-pmtna01.hosted.exlibrisgroup.com/primo-explore/search?"  enctype="application/x-www-form-urlencoded; charset=utf-8" method="get" id="primoSearchForm3" name="primoSearchForm3" role="search" target="_self"  onsubmit="searchPrimo3()" class="form-horizontal">
  <div class="form-group">
    <label for="primoQueryTemp3" class="sr-only">Books+ Search</label>
    <input type="hidden" name="institution" value="CALS_USM"/> 
    <input type="hidden" name="vid" value="CALS_USM"/> 
    <input type="hidden" name="tab" value="csusm_books"/> 
    <input type="hidden" name="search_scope" value="CSUSM_BOOKS"/> 
    <input type="hidden" name="mode" value="advanced"/> 
    <input type="hidden" name="onCampus" value="false"/> 
    <input type="hidden" id="primoQuery3" name="query" />
    <input type="hidden" id="indx" name="indx" value="1"/> 
    <input type="hidden" id="bulkSize" name="bulkSize" value="20"/> 
    <input type="hidden" id="dym" name="dym" value="true"/> 
    <input type="hidden" id="highlight" name="highlight" value="true"/> 
    <input type="hidden" id="displayField" name="displayField" value="title"/>
    <div class="col-sm-7">
      <input type="text" size="30" title="search_field" id="primoQueryTemp3" name="vl(freeText0)" class="form-control" value="" />
    </div>
    <div class="col-sm-2">
      <select class="EXLSelectTag blue EXLSimpleSearchSelect form-control" id="exlidInput_scope_1" name="vl(155172588UI0)">
        <option value="any" id="scope_any1" selected="selected" class="EXLSelectedOption">keyword</option>
        <option value="title" id="scope_title1" class="EXLSelectOption">title</option>
        <option value="creator" id="scope_creator1" class="EXLSelectOption">author</option>
        <option value="sub" id="scope_sub1" class="EXLSelectOption">subject</option>
        <option value="lsr10" id="scope_lsr101" class="EXLSelectOption">call#</option>
      </select>
    </div>
    <div class="">
      <button type="button" title="Search" class="btn btn-info" id="goButton" value="Search" name="primosearch" onclick="searchPrimo3()"/> <i class="fa fa-search"></i> Search</button>
    </div>
  </div>
  <div class="form-group">
    <div class="col-md-4 col-md-offset-8">
      <div id="browse-search-wrapper">
      </div>
      <div id="adv-search-wrapper">
        <a href="https://primo-pmtna01.hosted.exlibrisgroup.com/primo-explore/search?tab=csusm_books&search_scope=CSUSM_BOOKS&sortby=rank&vid=CALS_USM&lang=en_US&mode=advanced">Advanced Search</a>
      </div>
    </div>
  </div>
</form>
